﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private static GameManager instance;

    [HideInInspector]
    public const string FIRST_LEVEL_OF_THE_GAME = "Level 1";
    [HideInInspector]
    public const string LAST_LEVEL_OF_THE_GAME = "Level 3";

    [Header("Board")]
    public GameObject tilePrefab;
    public int boardSize;
    [HideInInspector]
    public List<List<Tile>> board = new List<List<Tile>>();
    [Header("Players")]
    public Transform chessMansParent;
    [HideInInspector]
    public int currentPlayerIndex = 0;
    public Transform playersParent;
    [HideInInspector]
    public List<Player> players;
    [HideInInspector]
    [Header("Pieces")]
    public ChessMan[, ] chessMans;
    [HideInInspector]
    public ChessMan selectedChessMan;
    public List<Tile> availableTilesForMovement;
    private List<Tile> availableTilesForAttacking;
    [Header("Audio")]
    [HideInInspector]
    public AudioSource gameAudioSource;
    public AudioClip victory;
    public AudioClip gameOver;
    public AudioClip levelComplete;
    [Header("User Interface")]
    public Text levelName;
    public Text score;
    public GameObject levelCompleteCanvas;
    public GameObject restartLevelCanvas;
    public GameObject gameVictoryCanvas;
    public Button musicButton;
    [Header("Victory Fireworks")]
    public ParticleSystem whiteFireworks;
    public ParticleSystem greenFireworks;
    public ParticleSystem redFireworks;

    public static GameManager GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start ()
    {   
        Time.timeScale = 1f;
        GenerateMap();
        GeneratePlayers();
        SpawnChessMans();

        PlayerPrefManager.SetHighscore(0);
        EnableGUIElements();
        RefreshGUI();
       
        gameAudioSource = gameObject.AddComponent<AudioSource>();
        gameAudioSource.loop = true;
        if (Camera.main.GetComponent<AudioSource>() != null && musicButton != null)
        {
            DesignOfMusicButton(Camera.main.GetComponent<AudioSource>().enabled);
        }  
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (players[currentPlayerIndex].isHuman)
        {
            players[currentPlayerIndex].Play();
        }
    }

    private void OnGUI()
    {
        players[currentPlayerIndex].TurnOnGUI(selectedChessMan);
    }

    private void GenerateMap()
    {
        board = new List<List<Tile>>();
        for (int x = 0; x < boardSize; x++)
        {
            List<Tile> row = new List<Tile>();
            for (int z = 0; z < boardSize; z++)
            {
                Tile tile = ((GameObject)Instantiate(tilePrefab, new Vector3(x, 0, z), Quaternion.Euler(new Vector3()))).GetComponent<Tile>();
                tile.gridPosition = new Vector2(x, z);
                row.Add(tile);
            }
            board.Add(row);

        }
    }

    private void GeneratePlayers()
    {
        players = new List<Player>();

        for (int i = 0; i < playersParent.childCount; i++)
        {
            Player p = playersParent.GetChild(i).GetComponent<Player>();
            if (p != null)
            {
                p.playerChessMans = new List<ChessMan>();
                players.Add(p);
            }
        }
    }

    private void SpawnChessMans()
    {
        chessMans = new ChessMan[boardSize, boardSize];

        for (int i = 0; i < chessMansParent.childCount; i++)
        {
            ChessMan c = chessMansParent.GetChild(i).GetComponent<ChessMan>();
            if(c != null)
            {
                c.SpawnChessMan(chessMansParent.GetChild(i).gameObject);
            }
        }
    }

    public void SelectChessMan(ChessMan c)
    {
        if (c.movesPerTurn > 0 || c.attacksPerTurn > 0)
        {
            RemoveTileHighlights();
            
            if (c.CanMove())
            {
                bool isChessManSelectedTwice = false;
                if(selectedChessMan != null && c.gridPosition == selectedChessMan.gridPosition)
                {
                    isChessManSelectedTwice = true;
                }
                DeselectChessMan();
                selectedChessMan = c;
                selectedChessMan.transform.GetComponent<Renderer>().material.color = Color.green;
                if (selectedChessMan.movesPerTurn > 0)
                {
                    HightlightTilesForMoving(selectedChessMan.PossibleMoves());
                    if (selectedChessMan.canAttackSimulteniously)
                    {
                        for (int x = 0; x < 8; x++)
                        {
                            for (int y = 0; y < 8; y++)
                            {
                                if (selectedChessMan.PossibleMoves()[x, y])
                                {
                                    Vector2 potentialPositionForAttack = new Vector2(x, y);
                                    HightlightTilesForAttacking(selectedChessMan.PossibleAttacks(potentialPositionForAttack));
                                }
                            }
                        }
                    }
                }
                if(selectedChessMan.attacksPerTurn > 0)
                {
                    HightlightTilesForAttacking(selectedChessMan.PossibleAttacks(selectedChessMan.gridPosition));
                    if(isChessManSelectedTwice && selectedChessMan.canAttackSimulteniously)
                    {
                        selectedChessMan.SimulteniousAttack();
                    }
                }
            }
        }
        else if(selectedChessMan.PossibleTilesToMove().Count == 0 && selectedChessMan.PossibleEnemiesForAttack().Count == 0)
        {
            selectedChessMan.movesPerTurn = 0;
            selectedChessMan.attacksPerTurn = 0;
            if (chessMans[(int)Mathf.Floor(selectedChessMan.gridPosition.x), (int)Mathf.Floor(selectedChessMan.gridPosition.y)] != null)
            {
                chessMans[(int)Mathf.Floor(selectedChessMan.gridPosition.x), (int)Mathf.Floor(selectedChessMan.gridPosition.y)].movesPerTurn = 0;
                chessMans[(int)Mathf.Floor(selectedChessMan.gridPosition.x), (int)Mathf.Floor(selectedChessMan.gridPosition.y)].attacksPerTurn = 0;
            }
            selectedChessMan.transform.GetComponent<Renderer>().material.color = players[currentPlayerIndex].chessManUsedColor;

            Debug.Log("The CHESSMAN HAS BEEN USED !");
        }
    }

    private void DeselectChessMan()
    {
        Color currentStateColor;

        if (selectedChessMan != null)
        {
            if (selectedChessMan.movesPerTurn == 0 && selectedChessMan.attacksPerTurn == 0)
            {
                currentStateColor = players[currentPlayerIndex].chessManUsedColor;
            }
            else
            {
                currentStateColor = players[currentPlayerIndex].playerColor;
            }

            selectedChessMan.transform.GetComponent<Renderer>().material.color = currentStateColor;
            selectedChessMan = null;
        }
    }

    private void HightlightTilesForMoving(bool[, ] availableMoves)
    {
        availableTilesForMovement = TileHightlight.FindHighlight(availableMoves);
        // If there are no available moves forbid moving of this chessMan
        if (availableTilesForMovement.Count == 0 && selectedChessMan != null)
        {
            selectedChessMan.movesPerTurn = 0;
        }
        else
        {
            foreach (Tile t in availableTilesForMovement)
            {
                t.transform.GetComponent<Renderer>().material.color = Color.cyan;
            }
        }
    }

    public void HightlightTilesForAttacking(bool[,] availableMoves)
    {
        availableTilesForAttacking = TileHightlight.FindHighlight(availableMoves);

        foreach (Tile t in availableTilesForAttacking)
        {
            t.transform.GetComponent<Renderer>().material.color = Color.yellow;
        }
    }

    public void RemoveTileHighlights()
    {
        for (int i = 0; i < boardSize; i++)
        {
            for (int j = 0; j < boardSize; j++)
            {
                if (!board[i][j].isImpassible)
                {
                    board[i][j].transform.GetComponent<Renderer>().material.color = Color.white;
                }
            }
        }
    }

    public void FindAvailablePathForSelectedChessMan(Tile destTile)
    {
        if(selectedChessMan != null && selectedChessMan.movesPerTurn > 0)
        {
            float chessManUpVectorFactor = selectedChessMan.transform.position.y;
            List<Tile> path = TilePathFinder.FindPath(board[(int)selectedChessMan.gridPosition.x][(int)selectedChessMan.gridPosition.y], destTile);

            foreach (Tile t in path)
            {
                selectedChessMan.positionQueue.Add(board[(int)t.gridPosition.x][(int)t.gridPosition.y].transform.position + chessManUpVectorFactor  * Vector3.up);
            }
            RemoveTileHighlights();
            selectedChessMan.gridPosition = destTile.gridPosition;
        }
    }

    public void ClearPreviousChessManPosition(ChessMan chessMan)
    {
        List<ChessMan> activeChessMans = new List<ChessMan>();
        for (int x = 0; x < boardSize; x++)
        {
            for (int y = 0; y < boardSize; y++)
            {
                if (chessMans[x, y] != null)
                {
                    activeChessMans.Add(chessMans[x, y]);
                }
            }
        }

        activeChessMans.Remove(chessMan);
        chessMans = new ChessMan[boardSize, boardSize];
        foreach (ChessMan c in activeChessMans)
        {
            chessMans[(int)Mathf.Floor(c.gridPosition.x), (int)Mathf.Floor(c.gridPosition.y)] = c;
        }
    }

    public void ResetAllChessMans()
    {
        DeselectChessMan();
        for (int x = 0; x < boardSize; x++)
        {
            for (int y = 0; y < boardSize; y++)
            {
                if (chessMans[x, y] != null && chessMans[x, y].playerCode == players[currentPlayerIndex].playerNumber)
                {
                    if(chessMans[x, y].hitPoints < chessMans[x, y].maximumHP)
                    {
                        chessMans[x, y].transform.GetComponent<Renderer>().material.color = players[currentPlayerIndex].chessManDamagedColor;
                    } else
                    { 
                        chessMans[x, y].transform.GetComponent<Renderer>().material.color = players[currentPlayerIndex].playerColor;
                    }
                    chessMans[x, y].movesPerTurn = 1;
                    chessMans[x, y].attacksPerTurn = 1;
                }

            }
        }
    }

    public void NextTurn()
    {
        DeselectChessMan();
        if (currentPlayerIndex + 1 < players.Count)
        {
            currentPlayerIndex++;
        }
        else
        {
            currentPlayerIndex = 0;
        }

        //Give the turn to the Ai
        if (!players[currentPlayerIndex].isHuman)
        {
            players[currentPlayerIndex].Play();
        }
    }


    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LevelComplete()
    {
        gameAudioSource.PlayOneShot(levelComplete);
        DisableGUIElements();
        Debug.Log("Level Complete ! Player " + players[currentPlayerIndex].playerName + " WINS !!! ");
        levelCompleteCanvas.SetActive(true);
        Time.timeScale = 0;
    }

    public void LevelFailed()
    {
        gameAudioSource.PlayOneShot(gameOver);
        DisableGUIElements();
        if (PlayerPrefManager.GetHighscore() < PlayerPrefManager.GetScore())
        {
            PlayerPrefManager.SetHighscore(PlayerPrefManager.GetScore());
        }
        Debug.Log("GAME OVER ! Player " + players[0].playerName + " LOST !!! ");
        restartLevelCanvas.SetActive(true);
        musicButton.gameObject.SetActive(false);
        Time.timeScale = 0;
    }

    public void GameCompleted()
    {
        gameAudioSource.PlayOneShot(victory);
        DisableGUIElements();
        Debug.Log("Congratulation ! Player " + players[currentPlayerIndex].playerName + " WINS !!! ");
        if (gameVictoryCanvas != null)
        {
            gameVictoryCanvas.SetActive(true);
        }
        
        if(whiteFireworks != null)
        {
            Instantiate(whiteFireworks, new Vector3(3, 1, 0), transform.rotation);
        }
        if(greenFireworks != null)
        {
            Instantiate(greenFireworks, new Vector3(4, 1, 0), transform.rotation);
        }
        if(redFireworks != null)
        {
            Instantiate(redFireworks, new Vector3(5, 1, 0), transform.rotation);
        }
        if (PlayerPrefManager.GetHighscore() < PlayerPrefManager.GetScore())
        {
            PlayerPrefManager.SetHighscore(PlayerPrefManager.GetScore());
        }
    }

    public void PlayStopMusic()
    {
        if(Camera.main.GetComponent<AudioSource>() != null)
        {
            if(Camera.main.GetComponent<AudioSource>().enabled == false)
            {
                Camera.main.GetComponent<AudioSource>().enabled = true;
            }
            else
            {
                Camera.main.GetComponent<AudioSource>().enabled = false;
            }
            DesignOfMusicButton(Camera.main.GetComponent<AudioSource>().enabled);
        }
    }

    private void DesignOfMusicButton(bool isSoundOn)
    {
        Text musicBtnText = null;
        if (musicButton.gameObject.transform.GetComponentInChildren<Text>() != null)
        {
            musicBtnText = musicButton.gameObject.transform.GetComponentInChildren<Text>();
        }
        if (isSoundOn)
        {
            if (musicBtnText != null)
            {
                musicBtnText.text = "Stop Music";
                musicBtnText.color = Color.white;
            }
            musicButton.gameObject.transform.GetComponent<Image>().color = Color.red;
        }
        else
        {
            if (musicBtnText != null)
            {
                musicBtnText.text = "Play Music";
                musicBtnText.color = Color.black;

            }
            musicButton.gameObject.transform.GetComponent<Image>().color = Color.green;
        }
    }

    private void RefreshGUI()
    {
        levelName.text = SceneManager.GetActiveScene().name;
        int startPointsForCurrentLevel = players[currentPlayerIndex].CalculateCurrentPoints();
        if (SceneManager.GetActiveScene().name.Equals(GameManager.FIRST_LEVEL_OF_THE_GAME))
        {
            SetStartPointsForFirstLevel(startPointsForCurrentLevel);
        }
        else
        {
            SetStartPoints(startPointsForCurrentLevel);
        }
    }

    private void EnableGUIElements()
    {
        levelName.gameObject.SetActive(true);
        score.gameObject.SetActive(true);
        musicButton.gameObject.SetActive(true);
        if (players[currentPlayerIndex].isHuman)
        {
            ((HumanPlayer)players[currentPlayerIndex]).endTurnButton.interactable = true;
        }
    }

    private void DisableGUIElements()
    {
        levelName.gameObject.SetActive(false);
        score.gameObject.SetActive(false);
        musicButton.gameObject.SetActive(false);
        if (players[0].isHuman)
        {
            ((HumanPlayer)players[currentPlayerIndex]).endTurnButton.interactable = false;
        }
    }

    private void SetStartPointsForFirstLevel(int startPoints)
    {
        PlayerPrefManager.ResetPlayerState(startPoints);
        int highscore = PlayerPrefManager.GetHighscore();
        if (highscore < startPoints)
        {
            highscore = startPoints;
            PlayerPrefManager.SetHighscore(highscore);
        }
        score.text = " SCORE BOARD \nScore: " + startPoints + "\nHighscore: " + highscore;
    }

    private void SetStartPoints(int nextLevelStartPoints)
    {
        nextLevelStartPoints += PlayerPrefManager.GetScore();
        int highscore = PlayerPrefManager.GetHighscore();
        if (highscore < nextLevelStartPoints)
        {
            highscore = nextLevelStartPoints;
            PlayerPrefManager.SetHighscore(highscore);
        }
        PlayerPrefManager.SetScore(nextLevelStartPoints);
        score.text = " SCORE BOARD \nScore: " + nextLevelStartPoints + "\nHighscore: " + highscore;
    }

    public void SetCurrentScore(int lostPoints)
    {
        int currentScore = PlayerPrefManager.GetScore() - lostPoints;
        PlayerPrefManager.SetScore(currentScore);
        score.text = " SCORE BOARD \nScore: " + currentScore + "\nHighscore: " + PlayerPrefManager.GetHighscore();
    }
}
