﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileHightlight  {

	public TileHightlight()
    {

    }

    public static List<Tile> FindHighlight (bool[,] availableMoves)
    {
        List<Tile> highlightedTiles = new List<Tile>();

        for(int x = 0; x < GameManager.GetInstance().boardSize; x++)
        {
            for(int y = 0; y < GameManager.GetInstance().boardSize; y++)
            {
                if (availableMoves[x, y])
                {
                    Tile tile = GameManager.GetInstance().board[x][y];
                    highlightedTiles.Add(tile);
                }
            }
        }

        return highlightedTiles;
    }
}
