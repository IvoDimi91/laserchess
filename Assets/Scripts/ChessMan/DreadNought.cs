﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DreadNought : ChessMan {

    public DreadNought()
    {
        playerCode = 1;
        chessManCode = 5;
        attackPower = 2;
        hitPoints = 5;
        maximumHP = 5;
        canAttackSimulteniously = true;
    }

    public override void SpawnChessMan(GameObject chessManPrefab)
    {
        base.SpawnChessMan(chessManPrefab);
    }

    public override bool[,] PossibleMoves()
    {
        bool[,] pM = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        int maxPermitedSpaces = 1;

        ChessMan c;

        // Right
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        while (maxPermitedSpaces > 0)
        {
            x++;
            if (x >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Left
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {
            x--;
            if (x < 0)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Up
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {
            y++;
            if (y >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Down
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {
            y--;
            if (y < 0)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //TopLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x--;
            y++;

            if (x < 0 || y >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //TopRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x++;
            y++;

            if (x >= 8 || y >= 8)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //DownLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x--;
            y--;

            if (x < 0 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //DownRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x++;
            y--;

            if (x >= 8 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        return pM;
    }

    public override bool[,] PossibleAttacks(Vector2 gridPosition)
    {
        bool[,] pA = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        int maxPermitedSpaces = 1;

        ChessMan c;

        // Right
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        while (maxPermitedSpaces > 0)
        {
            x++;
            if (x >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Left
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {
            x--;
            if (x < 0)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Up
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {
            y++;
            if (y >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Down
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {
            y--;
            if (y < 0)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //TopLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x--;
            y++;

            if (x < 0 || y >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //TopRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x++;
            y++;

            if (x >= 8 || y >= 8)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //DownLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x--;
            y--;

            if (x < 0 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //DownRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 1;
        while (maxPermitedSpaces > 0)
        {

            x++;
            y--;

            if (x >= 8 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && this.playerCode != c.playerCode)
            {
                pA[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        return pA;
    }

    public override void MoveChessMan()
    {
        base.MoveChessMan();
    }

    public override void AttackOpponentsChessMan(List<ChessMan> opponentChessMans)
    {
        base.AttackOpponentsChessMan(opponentChessMans);
    }
}
