﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChessMan : MonoBehaviour {

    public int chessManCode;
    public int playerCode;
    public string chessManName;
    [HideInInspector]
    public Vector2 gridPosition = Vector2.zero;
    public Vector3 spawnPosition;
    public float moveSpeed = 10f;

    //How many moves per turn
    public int movesPerTurn = 1;
    //How many attacks per turn
    public int attacksPerTurn = 1;
    public int attackPower = 1;
    public int hitPoints = 1;
    public int maximumHP = 1;
    public bool canAttackSimulteniously = false;
    public ParticleSystem chessManDead;
    public ParticleSystem chessManInjured;
    public AudioClip chessManMoveSFX;
    public AudioClip chessManAttackSFX;
    //movement animation;
    [HideInInspector]
    public List<Vector3> positionQueue = new List<Vector3>();

    public int chessManValue;

    private void OnMouseEnter()
    {
        if(transform.Find("Canvas") != null)
        {
            //Transform textGO = transform.GetChild(0).Find("Text");
            //transform.Find("Canvas").parent = transform;
            Text text = transform.Find("Canvas").GetComponent<Text>();
            text.text = "ChessMan: " + chessManName 
                        +"\nDamage: " + attackPower
                        +"\nRemaining Health: " + hitPoints + " HP";
            //text.color = GetComponent<Renderer>().material.color;
            text.enabled = true;
        }
    }

    private void OnMouseExit()
    {
        if (transform.Find("Canvas") != null)
        {
            Text text = transform.Find("Canvas").GetComponent<Text>();
            text.text = "";
            text.enabled = false;
        }
    }

    public virtual void SpawnChessMan(GameObject chessManPrefab)
    {
        ChessMan c = ((GameObject)Instantiate(chessManPrefab, spawnPosition, Quaternion.Euler(new Vector3()))).GetComponent<ChessMan>();
        c.gridPosition = new Vector2(spawnPosition.x, spawnPosition.z);
        GameManager.GetInstance().chessMans[(int)Mathf.Floor(spawnPosition.x), (int)Mathf.Floor(spawnPosition.z)] = c;
        GameManager.GetInstance().players[c.playerCode].playerChessMans.Add(c);
    }

    public virtual bool[,] PossibleMoves()
    {
        return new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
    }

    public virtual bool[,] PossibleAttacks(Vector2 gridPosition)
    {
        return new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
    }

    public virtual void MoveChessMan()
    {
        Player currentPlayer = GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex];
        if (positionQueue.Count > 0)
        {
            if (transform.GetComponent<ChessMan>().chessManCode == (int) ChessMan.ChessManCode.JUMPSHIP)
            {
                if (Vector3.Distance(positionQueue[0], transform.position) > 0.1f)
                {
                    transform.position += (positionQueue[0] - transform.position).normalized * moveSpeed * Time.deltaTime;

                    if (Vector3.Distance(positionQueue[0], transform.position) <= 0.1f)
                    {
                        transform.position = positionQueue[0];
                        positionQueue.RemoveAt(0);
                        if (positionQueue.Count == 0)
                        {
                            GameManager.GetInstance().ClearPreviousChessManPosition(this);
                            movesPerTurn--;
                            GameManager.GetInstance().chessMans[(int)Mathf.Floor(gridPosition.x), (int)Mathf.Floor(gridPosition.y)] = this;
                            GameManager.GetInstance().gameAudioSource.PlayOneShot(chessManMoveSFX);
                        }
                    }
                }
            }
            else
            {
                GameManager.GetInstance().ClearPreviousChessManPosition(this);
                transform.position = positionQueue[positionQueue.Count - 1];
                positionQueue.Clear();
                movesPerTurn--;
                GameManager.GetInstance().chessMans[(int)Mathf.Floor(gridPosition.x), (int)Mathf.Floor(gridPosition.y)] = this;
                GameManager.GetInstance().gameAudioSource.PlayOneShot(chessManMoveSFX);
            }
        }
        if (this.movesPerTurn == 0) {
            if (this.attacksPerTurn > 0 && this.PossibleEnemiesForAttack().Count > 0)
            {
                if (this.canAttackSimulteniously)
                {
                    this.SimulteniousAttack();
                } else
                {
                    GameManager.GetInstance().HightlightTilesForAttacking(PossibleAttacks(this.gridPosition));
                }
            } else
            {
                this.attacksPerTurn = 0;
                this.transform.GetComponent<Renderer>().material.color = currentPlayer.chessManUsedColor;
                GameManager.GetInstance().RemoveTileHighlights();
            }
        }
    }

    public virtual void AttackOpponentsChessMan(List<ChessMan> opponentChessMans)
    {
        Player currentPlayer = GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex];
        if (opponentChessMans.Count == 0)
        {
            this.transform.GetComponent<Renderer>().material.color = currentPlayer.chessManUsedColor;
            return;
        }

        foreach (ChessMan opponentChessMan in opponentChessMans)
        {
            opponentChessMan.hitPoints -= this.attackPower;
            if (opponentChessMan.hitPoints <= 0)
            {
                if (chessManDead != null)
                {
                    Instantiate(opponentChessMan.chessManDead, opponentChessMan.transform.position, opponentChessMan.transform.rotation);
                }
                opponentChessMan.DestroyChessMan(currentPlayer);
            }
            else
            {
                if (chessManInjured != null)
                {
                    Instantiate(opponentChessMan.chessManInjured, opponentChessMan.transform.position, opponentChessMan.transform.rotation);
                }
                opponentChessMan.GetComponent<Renderer>().material.color = currentPlayer.isHuman ? GameManager.GetInstance().players[1].chessManDamagedColor : GameManager.GetInstance().players[0].chessManDamagedColor;
            }
        }
        attacksPerTurn--;
        if (chessManAttackSFX != null)
        {
            GameManager.GetInstance().gameAudioSource.PlayOneShot(chessManAttackSFX);
        }
        if (attacksPerTurn == 0)
        {
            if (movesPerTurn > 0)
            {
                movesPerTurn = 0;
            }
            this.transform.GetComponent<Renderer>().material.color = currentPlayer.chessManUsedColor;
            GameManager.GetInstance().RemoveTileHighlights();
        }
    }

    public virtual bool CanMove()
    {
        return true;
    }

    public virtual void DestroyChessMan(Player currentPlayer)
    {
        Destroy(this.gameObject);
        Destroy(GameManager.GetInstance().chessMans[(int)Mathf.Floor(gridPosition.x), (int)Mathf.Floor(gridPosition.y)].gameObject);
        GameManager.GetInstance().chessMans[(int)Mathf.Floor(gridPosition.x), (int)Mathf.Floor(gridPosition.y)] = null;

        Player opponent = currentPlayer.isHuman ? GameManager.GetInstance().players[1] : GameManager.GetInstance().players[0];

        if (this.playerCode == opponent.playerNumber && opponent.playerChessMans.Contains(this)) {
            opponent.playerChessMans.Remove(this);

            if (opponent.isHuman)
            {
                GameManager.GetInstance().SetCurrentScore(this.chessManValue);
                if (opponent.playerChessMans.Count == 0)
                {
                    GameManager.GetInstance().LevelFailed();
                }
            } else
            {
                int commandUnitsCounter = 0;
                foreach(ChessMan c in opponent.playerChessMans)
                {
                    if(c != null && c.GetType() == typeof(CommandUnit))
                    {
                        commandUnitsCounter++;
                        break;
                    }
                }
                if(commandUnitsCounter == 0)
                {
                    if (SceneManager.GetActiveScene().name.Equals(GameManager.LAST_LEVEL_OF_THE_GAME))
                    {
                        GameManager.GetInstance().GameCompleted();
                    }
                    else
                    {
                        GameManager.GetInstance().LevelComplete();
                    }
                }
            }
        }
    }

    public void SimulteniousAttack()
    {
        List<ChessMan> opponentChessMans = PossibleEnemiesForAttack();

        if (opponentChessMans.Count > 0)
        {
            this.AttackOpponentsChessMan(opponentChessMans);
        }
    }

    public List<Tile> PossibleTilesToMove()
    {
        List<Tile> availableTiles = new List<Tile>();
        bool[,] posibleMoves = PossibleMoves();
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                Tile tile = GameManager.GetInstance().board[x][y];
                if (posibleMoves[x, y] && tile != null)
                {
                    availableTiles.Add(tile);
                }
            }
        }
        return availableTiles;
    }

    public List<ChessMan> PossibleEnemiesForAttack()
    {
        List<ChessMan> opponentChessMans = new List<ChessMan>();
        bool[,] possibleAttacks = PossibleAttacks(gridPosition);
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                ChessMan opponentChessMan = GameManager.GetInstance().chessMans[x, y];
                if (possibleAttacks[x, y] && opponentChessMan != null && opponentChessMan.playerCode != this.playerCode)
                {
                    opponentChessMans.Add(opponentChessMan);
                }
            }
        }
        return opponentChessMans;
    }

    public enum ChessManCode
    {
        GRUNT = 1,
        JUMPSHIP = 2,
        TANK = 3,
        DRONE = 4,
        DREADNOUGHT = 5,
        COMMANDUNIT = 6
    }
}
