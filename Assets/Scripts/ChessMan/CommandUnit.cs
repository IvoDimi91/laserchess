﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandUnit : ChessMan {

    public CommandUnit()
    {
        playerCode = 1;
        chessManCode = 6;
        attackPower = 0;
        hitPoints = 5;
        maximumHP = 5;
    }

    public override void SpawnChessMan(GameObject chessManPrefab)
    {
        base.SpawnChessMan(chessManPrefab);
    }

    public override bool[,] PossibleMoves()
    {
        bool[,] pM = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        //left
        x = (int)Mathf.Floor(gridPosition.x) - 1;
        y = (int)Mathf.Floor(gridPosition.y);
        if (x >= 0 && GameManager.GetInstance().chessMans[x, y] == null)
        {
            pM[x, y] = true;
        }

        //right
        x = (int)Mathf.Floor(gridPosition.x) + 1;
        y = (int)Mathf.Floor(gridPosition.y);
        if (x <= 7 && GameManager.GetInstance().chessMans[x, y] == null)
        {
            pM[x, y] = true;
        }

        return pM;
    }

    public override void MoveChessMan()
    {
        base.MoveChessMan();
    }

    public override void AttackOpponentsChessMan(List<ChessMan> opponentChessMans)
    {
        base.AttackOpponentsChessMan(opponentChessMans);
    }

    public override bool CanMove()
    {
        List<ChessMan> dreadNoughts = new List<ChessMan>();
        ChessMan[,] chessMans = GameManager.GetInstance().chessMans;

        for( int x = 0; x < GameManager.GetInstance().boardSize; x++)
        {
            for (int y = 0; y < GameManager.GetInstance().boardSize; y++)
            {
                if(chessMans[x, y] != null && chessMans[x, y].chessManName.Equals("DreadNought", StringComparison.CurrentCultureIgnoreCase))
                {
                    dreadNoughts.Add(chessMans[x, y]);
                }
            }
        }

        if(dreadNoughts.Count > 0)
        {
            foreach(ChessMan dN in dreadNoughts)
            {
                if(dN.movesPerTurn > 0)
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
