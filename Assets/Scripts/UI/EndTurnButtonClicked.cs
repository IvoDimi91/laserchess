﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTurnButtonClicked : MonoBehaviour {

    public void OnClick()
    {
        GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex].EndTurn();
    }
}
