﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicButtonClicked : MonoBehaviour {


    public void OnClick()
    {
        GameManager.GetInstance().PlayStopMusic();
    }
}
