﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public string playerName;
    public int playerNumber;
    [HideInInspector]
    public List<ChessMan> playerChessMans;
    public bool isHuman;
    public Color playerColor;
    public Color chessManUsedColor;
    public Color chessManDamagedColor;
    [HideInInspector]
    public int playerPoints;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void Play()
    {

    }

    public virtual void TurnOnGUI(ChessMan selectedChessMan)
    {

    }

    public virtual void EndTurn()
    {

    }

    public virtual int CalculateCurrentPoints()
    {
        int points = 0;
        foreach (ChessMan chessMan in playerChessMans)
        {
            points += chessMan.chessManValue;
        }
        return points;
    }
}
