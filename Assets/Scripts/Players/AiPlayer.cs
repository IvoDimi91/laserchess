﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AiPlayer : Player {

    private List<ChessMan> drones;
    private List<ChessMan> dreadNoughts;
    private List<ChessMan> commandUnits;
    private List<ChessMan> enemyChessMans;

    private System.Random rnd;

    public AiPlayer()
    {
        rnd = new System.Random();
        isHuman = false;
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Play()
    {
        base.Play();
        
        drones = new List<ChessMan>();
        dreadNoughts = new List<ChessMan>();
        commandUnits = new List<ChessMan>();
        enemyChessMans = new List<ChessMan>();
        ChessMan[,] chessMans = GameManager.GetInstance().chessMans;

        for (int x = 0; x < GameManager.GetInstance().boardSize; x++)
        {
            for (int y = 0; y < GameManager.GetInstance().boardSize; y++)
            {
                if (chessMans[x, y] != null)
                {
                    if (chessMans[x, y].playerCode != this.playerNumber)
                    {
                        enemyChessMans.Add(chessMans[x, y]);
                    }
                    else
                    {
                        if (chessMans[x, y].chessManCode == (int) ChessMan.ChessManCode.DRONE)
                        {
                            drones.Add(chessMans[x, y]);
                        }
                        else if (chessMans[x, y].chessManCode == (int) ChessMan.ChessManCode.DREADNOUGHT)
                        {
                            dreadNoughts.Add(chessMans[x, y]);
                        }
                        else
                        {
                            commandUnits.Add(chessMans[x, y]);
                        }
                    }
                }
            }
        }

        StartCoroutine(StartAiTurn());
    }

    private IEnumerator StartAiTurn()
    {
        //Drones
        if (drones != null && drones.Count > 0)
        {
            foreach (ChessMan drone in drones)
            {
                GameManager.GetInstance().selectedChessMan = drone;
                List<Tile> availableMovePositions = new List<Tile>();

                for (int x = 0; x < GameManager.GetInstance().boardSize; x++)
                {
                    for (int y = 0; y < GameManager.GetInstance().boardSize; y++)
                    {
                        if (drone.PossibleMoves()[x, y])
                        {
                            Tile tile = GameManager.GetInstance().board[x][y];
                            availableMovePositions.Add(tile);
                        }
                    }
                }

                if (availableMovePositions.Count > 0)
                {
                    int tileIndex = rnd.Next(0, availableMovePositions.Count);
                    
                    if (drone.movesPerTurn > 0)
                    {
                        GameManager.GetInstance().FindAvailablePathForSelectedChessMan(availableMovePositions[tileIndex]);
                        yield return new WaitForSeconds(0.5f);
                        drone.MoveChessMan();
                    }
                } else
                {
                    drone.movesPerTurn = 0;
                }
                yield return new WaitForSeconds(0.5f);

                //ATTACK !
                List<ChessMan> availableEnemiesForAttack = drone.PossibleEnemiesForAttack();

                if (availableEnemiesForAttack.Count > 0 && drone.attacksPerTurn > 0)
                {
                    List<ChessMan> chessMansWithLowestHP = new List<ChessMan>();

                    /* Check if the Drone can kill enemy. In my opinion this should be the best strategy for them.
                     * Since they do only 1 damage I deside not to search for the Tanks all the time, because the are very strong and the Human player will value them a lot.
                     * Instead, I program the drones to hunt for figures, which might be killed easily. 
                     * Thiss will brind the Ai player closer to his victory condition.
                    */
                    ChessMan enemyWithLowestHP = null;
                    int maxRemainingHPAfterAttack = 2;
                    foreach(ChessMan enemy in availableEnemiesForAttack)
                    {
                        //Choose target for attack
                        if(Mathf.Abs(drone.attackPower - enemy.hitPoints) <= maxRemainingHPAfterAttack)
                        {
                            enemyWithLowestHP = enemy;
                            maxRemainingHPAfterAttack = Mathf.Abs(drone.attackPower - enemy.hitPoints);
                        }
                        //Check if there is low hp opponent chessMan with higher value. 
                        if(maxRemainingHPAfterAttack <= 1)
                        {
                            if (enemyWithLowestHP != null && enemyWithLowestHP.hitPoints <= enemy.hitPoints && enemyWithLowestHP.chessManCode < enemy.chessManCode)
                            {
                                enemyWithLowestHP = enemy;
                                maxRemainingHPAfterAttack = Mathf.Abs(drone.attackPower - enemy.hitPoints);
                            }
                        }
                    }
                    if (enemyWithLowestHP != null)
                    {
                        chessMansWithLowestHP.Add(enemyWithLowestHP);
                    }
                    else
                    {
                        //If there is no best target choose one Randomly
                        int tileIndex = rnd.Next(0, availableEnemiesForAttack.Count);
                        chessMansWithLowestHP.Add(availableEnemiesForAttack[tileIndex]);
                    }
                    drone.AttackOpponentsChessMan(chessMansWithLowestHP);
                }
                else
                {
                    drone.attacksPerTurn = 0;
                }
                drone.transform.GetComponent<Renderer>().material.color = GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex].chessManUsedColor;
                yield return new WaitForSeconds(0.5f);
            }
        }

        //DreadNoughts
        if (dreadNoughts != null && dreadNoughts.Count > 0)
        {
            foreach (ChessMan dreadNought in dreadNoughts)
            {
                GameManager.GetInstance().selectedChessMan = dreadNought;
                List<Tile> availableMovePositions = new List<Tile>();

                for (int x = 0; x < GameManager.GetInstance().boardSize; x++)
                {
                    for (int y = 0; y < GameManager.GetInstance().boardSize; y++)
                    {
                        if (dreadNought.PossibleMoves()[x, y])
                        {
                            Tile tile = GameManager.GetInstance().board[x][y];
                            availableMovePositions.Add(tile);
                        }
                    }
                }

                if (availableMovePositions.Count > 0)
                {
                    //Search for closest position to enemy unit
                    float distance = 6f;
                    Tile bestMove = availableMovePositions[0];

                    foreach (ChessMan enemy in enemyChessMans)
                    {
                        foreach (Tile possibleMove in availableMovePositions)
                        {
                            if (enemy != null && distance > Vector3.Distance(enemy.transform.position, possibleMove.transform.position))
                            {
                                distance = Vector3.Distance(enemy.transform.position, possibleMove.transform.position);
                                bestMove = possibleMove;
                            }
                        }
                    }

                    if (dreadNought.movesPerTurn > 0 && bestMove != null)
                    {
                        GameManager.GetInstance().FindAvailablePathForSelectedChessMan(bestMove);
                        yield return new WaitForSeconds(0.5f);
                        dreadNought.MoveChessMan();
                    }
                }

                yield return new WaitForSeconds(0.5f);

                //ATTACK !
                List<ChessMan> availableEnemiesForAttack = dreadNought.PossibleEnemiesForAttack();

                if (availableEnemiesForAttack.Count > 0 && dreadNought.attacksPerTurn > 0)
                {
                    dreadNought.AttackOpponentsChessMan(availableEnemiesForAttack);
                }
                else
                {
                    dreadNought.attacksPerTurn = 0;
                }
                dreadNought.transform.GetComponent<Renderer>().material.color = GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex].chessManUsedColor;
                yield return new WaitForSeconds(0.5f);
            }
        }

        //CommandUnits
        if (commandUnits != null && commandUnits.Count > 0)
        {
            foreach (ChessMan commandUnit in commandUnits)
            {
                GameManager.GetInstance().selectedChessMan = commandUnit;
                List<Tile> availableMovePositions = new List<Tile>();

                for (int x = 0; x < GameManager.GetInstance().boardSize; x++)
                {
                    for (int y = 0; y < GameManager.GetInstance().boardSize; y++)
                    {
                        if (commandUnit.PossibleMoves()[x, y])
                        {
                            Tile tile = GameManager.GetInstance().board[x][y];
                            availableMovePositions.Add(tile);
                        }
                    }
                }

                //Deffence strategy to protect the Command Unit
                Tile bestMove = null;
                foreach (ChessMan enemy in enemyChessMans)
                {
                    foreach (Tile possibleMove in availableMovePositions)
                    {
                        if (enemy != null)
                        {
                            //Command Unit is in danger
                            if (enemy.PossibleAttacks(enemy.gridPosition)[(int)Mathf.Floor(commandUnit.gridPosition.x), (int)Mathf.Floor(commandUnit.gridPosition.y)])
                            {
                                if (enemy.PossibleAttacks(enemy.gridPosition)[(int)Mathf.Floor(possibleMove.gridPosition.x), (int)Mathf.Floor(possibleMove.gridPosition.y)])
                                {
                                    if(enemy.attackPower < 2)
                                    {
                                        //Take Hit from less powerfull enemy         
                                        bestMove = possibleMove;
                                    }
                                }
                                else
                                {
                                    //Found save place
                                    bestMove = possibleMove;
                                }
                                //Can not escape from heavy attack. For example, this happens when a Tank appears Command Unit's row 
                            }
                        }
                    }
                }

                if (commandUnit.movesPerTurn > 0 && bestMove != null)
                {
                    GameManager.GetInstance().FindAvailablePathForSelectedChessMan(bestMove);
                    yield return new WaitForSeconds(0.5f);
                    commandUnit.MoveChessMan();
                }
                commandUnit.transform.GetComponent<Renderer>().material.color = GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex].chessManUsedColor;
                yield return new WaitForSeconds(0.5f);
            }
        }
        EndTurn();
    }

    public override void EndTurn()
    {
        base.EndTurn();
        GameManager.GetInstance().RemoveTileHighlights();
        GameManager.GetInstance().ResetAllChessMans();
        GameManager.GetInstance().NextTurn();
    }
}
